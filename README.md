# golang-build-docker

Docker image for building golang projects with extras like protobuf support.

## Versions
* Golang: __1.21.0__
* Protoc: __24.2__

## Docker build commands

```
export TAG=xx.xx.xx
docker build -t mshindle/golang-extra:${TAG} .
docker push mshindle/golang-extra:${TAG}
docker tag mshindle/golang-extra:${TAG} mshindle/golang-extra:latest
```
dock