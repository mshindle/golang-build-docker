FROM golang:1.21.0-bullseye

RUN apt-get update -y && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends unzip && \
    rm -rf /var/lib/apt/lists/*

ENV PROTOBUF_TAG=24.2
COPY --chmod=777 build.sh /tmp/build.sh
RUN /tmp/build.sh
