#!/bin/sh

set -e

DIR=v${PROTOBUF_TAG}
FILE=protoc-${PROTOBUF_TAG}-linux-x86_64.zip

cd /tmp
wget https://github.com/protocolbuffers/protobuf/releases/download/${DIR}/${FILE}
cd /usr/local
unzip /tmp/$FILE

go install google.golang.org/protobuf/cmd/protoc-gen-go@latest || exit 1
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest || exit 1
